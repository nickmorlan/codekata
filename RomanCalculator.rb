class RomanCalculator
    NUMERALS = ['M', 'D', 'C', 'L', 'X', 'V', 'I']
    LONG_FORM = {'IV'=> 'IIII',
                 'IX'=>'VIIII',
                 'XL'=>'XXXX',
                 'CD'=>'CCCC',
                 'CM'=>'DCCCC'}
    NEXT_NUMERAL = {'IIIII'=>'V',
                    'XXXXX' => 'L',
                    'CCCCC' => 'D'}
    NEXT_POWER_TEN = {'VV'=>'X',
                     'LL' => 'C',
                     'DD' => 'M'}              
    attr_accessor :total, :first, :second

    def add(one, two)
        @first = long_form(one)
        @second = long_form(two)
        
        get_total
    end

    private
        # swap modifiers for the long version -- ie IV = IIII
        def long_form(value)
            LONG_FORM.each_with_index { |(key, val), index| value.gsub! key.to_s, val }
            value
        end

        #count of each of the numerals and return the count in a hash
        def long_totals 
           (@first + @second).scan(/\w/).inject(Hash.new(0)) {|h,c| h[c] +=1; h}
        end

        # long form total 
        def get_total
            long = long_totals
            @total = NUMERALS.inject('') { |total, n| total += n * long[n] }
            shorten(@total)
            
        end

        # swap long numeral strings to the next value
        def shorten(value)
            NEXT_NUMERAL.each_with_index { |(key, val), index| value.gsub! /(.*)#{key.to_s}(.*)/, '\1' + val + '\2' }
            NEXT_POWER_TEN.each_with_index { |(key, val), index| value.gsub! key.to_s, val }
            LONG_FORM.to_a.reverse.to_h.each_with_index { |(key, val), index| value.gsub! val, key.to_s }
            value
        end
end