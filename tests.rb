require 'minitest/autorun'
require './RomanCalculator'

class TestRomanCalculator < MiniTest::Test

    def setup
        @calculator = RomanCalculator.new
    end

    def test_add_method_present
        assert_respond_to @calculator, :add
    end

    def test_long_form_quick_check
        assert_equal "CXXXX", (@calculator.send(:long_form, "CXL"))
    end

    def test_long_totals
        @calculator.first = 'CXXXX'
        @calculator.second = 'XVIII'
        assert_equal ({"C"=>1, "X"=>5, "V"=>1, "I"=>3}), @calculator.send(:long_totals)
    end


    def test_get_total
        @calculator.first = 'VIIII'
        @calculator.second = 'XVI'

        assert_equal 'XXV', @calculator.send(:get_total)
    end
end